#  File: Spiral.py

#  Description: Generates a grid of numbers that spiral out from the center
#increasing in value, then provides the section of the grid surrounding
#a specified number

#  Student's Name: Christopher Calizzi

#  Student's UT EID:csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created:2-13-20

#  Date Last Modified: 2-13-20


#  Input: dim is a positive odd integer
#  Output: function returns a 2-D list of integers arranged
#          in a spiral
def create_spiral (dim):
    value = dim ** 2
    rows = []
    for i in range(dim):
        rows.append([0]*dim)
    row = 0
    radius = dim
    min_index = 0
    while value:
        for column in range(radius - 1, min_index,-1):
            rows[row][column] = value
            value -=1
        column = min_index
        if value:
            for row in range(min_index, radius-1):
                rows[row][column] = value
                value -=1
            row = radius-1
        if value:
            for column in range(min_index, radius-1):
                rows[row][column] = value
                value -= 1
            column = radius-1
        if value:
            for row in range(radius - 1, min_index,-1):
                rows[row][column] = value
                value -=1
        radius -=1
        min_index +=1
        if value == 1:
            rows[radius-1][radius-1] = 1
            value -=1
    return rows
#  Input: grid a 2-D list containing a spiral of numbers
#         val is a number withing the range of numbers in
#         the grid
#  Output: sub-grid surrounding the parameter val in the grid
#          sub-grid could be 1-D or 2-D list
def sub_grid (grid, val):
    index = 0
    row = grid[index]
    while not val in row:
        index+=1
        row = grid[index]
    sub_grid = []
    col = row.index(val)
    row = index
    low_col = col==0
    high_col = col==len(grid)-1
    low_row = row==0
    high_row = row==len(grid)-1
    if not(low_col or low_row or high_col or high_row):
        for i in range(3):
            sub_grid.append([0,0,0])
        for i in range(-1,2):
            for l in range(-1,2):
                sub_grid[i+1][l+1] = grid[row+i][col+l]
    elif low_col:
        if high_row:
            for i in range(2):
                sub_grid.append([0, 0])
            for i in range(-1, 1):
                for l in range(0, 2):
                    sub_grid[i + 1][l] = grid[row + i][col + l]
        elif low_row:
            for i in range(2):
                sub_grid.append([0, 0])
            for i in range(0, 2):
                for l in range(0, 2):
                    sub_grid[i][l] = grid[row + i][col + l]
        else:
            for i in range(3):
                sub_grid.append([0, 0])
            for i in range(-1, 2):
                for l in range(0, 2):
                    sub_grid[i+1][l] = grid[row + i][col + l]
    elif high_col:
        if high_row:
            for i in range(2):
                sub_grid.append([0, 0])
            for i in range(-1, 1):
                for l in range(-1, 1):
                    sub_grid[i + 1][l+1] = grid[row + i][col + l]
        elif low_row:
            for i in range(2):
                sub_grid.append([0, 0])
            for i in range(0, 2):
                for l in range(-1, 1):
                    sub_grid[i][l+1] = grid[row + i][col + l]
        else:
            for i in range(3):
                sub_grid.append([0, 0])
            for i in range(-1, 2):
                for l in range(-1, 1):
                    sub_grid[i+1][l+1] = grid[row + i][col + l]
    elif low_row:
        for i in range(2):
            sub_grid.append([0, 0, 0])
        for i in range(0, 2):
            for l in range(-1, 2):
                sub_grid[i][l + 1] = grid[row + i][col + l]
    else:
        for i in range(2):
            sub_grid.append([0, 0, 0])
        for i in range(-1, 1):
            for l in range(-1, 2):
                sub_grid[i + 1][l + 1] = grid[row + i][col + l]
    return sub_grid




def main():
  # prompt user to enter dimension of grid
        dimension = int(input("Enter dimension: "))
        # prompt user to enter value in grid
        number = int(input("Enter number in spiral: "))
        # print subgrid surrounding the value
        spiral = create_spiral(dimension)
        grid = sub_grid(spiral,number)
        for row in grid:
            for num in row:
                print(num, end = " ")
            print()
if __name__ == "__main__":
  main()